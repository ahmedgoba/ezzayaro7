<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\models\Place;
use App\models\PlacesForReview;
use App\models\PlaceName;


class PlaceController extends Controller
{
    public function index(Request $request)
    {
        $places = Place::paginate(5);
        return view('apis.places.index', ['places' => $places]);
    }
    public function get(Request $request)
    {
       
    }
    public function search(Request $request)
    {
        $place1=$request->input('place1');
        $place2=$request->input('place2');
        $places_result = DB::table('places')->where([
            ['start_place', '=', $place1],
            ['dist_place', '=', $place2]
        ])->get();
        #dd($places_result);

        $places_names = DB::table('place_names')->distinct()->pluck('name');
       
       
        return view('pages.website.search_results',[
            'places_names' => $places_names,
            'places_result'=>$places_result,
            'place1'=>$place1,
            'place2'=>$place2]);
    }

    public function create(Request $request,PlaceForReview $placeForReview)
    {
        $place=new Place;
        $place->start_place=$placeForReview->start_place;
        $place->dist_place=$placeForReview->dist_place;
        $place->discription=$placeForReview->discription;
        $place->user_id=$placeForReview->user_id;
        $place->save();

        $place_name = PlaceName::firstOrNew(array('name' => $place->start_place));
        $place_name->save();
        
        $place_name = PlaceName::firstOrNew(array('name' => $place->dist_place));
        $place_name->save();

        return $place;
    }
  
}
