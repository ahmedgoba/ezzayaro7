<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

use App\models\PlacesForReview;
use App\models\PlaceName;
use App\models\Place;

class PlacesReviewController extends Controller
{
    public function index(Request $request)
    {
        
    }
    public function get(Request $request)
    {
       
    }

    public function create(Request $request)
    {
        $attributes = [
            'start_place' => 'Start Place',
            'dist_place'  => 'Distination Place',
            'discription'  => 'Description',
        ];
        $validator = Validator::make($request->all(), [
            'name' => ['required'],
            'email' => ['required'],
            'start_place' => ['required'],
            'dist_place' => ['required','min:5'],
            'discription' => ['required','min:5'],
        ],
        [],$attributes);
        if ($validator->fails()) {
            return redirect('add-place')
                        ->withErrors($validator)
                        ->withInput();
        }

        $placeForReview=new PlacesForReview;
        $placeForReview->start_place=$request->input('start_place');
        $placeForReview->dist_place=$request->input('dist_place');
        $placeForReview->discription=$request->input('discription');
        if (Auth::check()) {
            $placeForReview->user_id=Auth::id();
        }
        $placeForReview->save();


        return view('pages.website.thank_you');
    }
    public function confirm(Request $request,$id)
    {
        $placeForReview=PlacesForReview::find($id);
        $place=new Place;
        $place->start_place=$placeForReview->start_place;
        $place->dist_place=$placeForReview->dist_place;
        $place->discription=$placeForReview->discription;
        $place->user_id=$placeForReview->user_id;
        $place->save();

        $place_name = PlaceName::firstOrNew(array('name' => $place->start_place));
        $place_name->save();
        
        $place_name = PlaceName::firstOrNew(array('name' => $place->dist_place));
        $place_name->save();

        $placeForReview->delete();
        return back();
    }
}
