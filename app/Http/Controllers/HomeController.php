<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\models\PlaceName;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $places_names = DB::table('place_names')->distinct()->pluck('name');
        return view('main.index',['places_names' => $places_names]);
    }
}
