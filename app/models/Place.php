<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    public function user()
    {
        return $this->hasOne('App\User');
    }
}
