<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlacesForReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('places_for_reviews', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean('confirmed')->default(0);
            $table->boolean('rejected')->default(0);
            $table->string('start_place');
            $table->string('dist_place');
            $table->text('discription');
            $table->bigInteger('reputation')->default(0);
            $table->bigInteger('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('places_for_reviews');
    }
}
