@extends('main.main-layout')
    @section('title','Ezzay Aro7')
    
    @section('styles')
        <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}" >
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
        <link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
    @endsection
    
    @section('header')
        @include('main.header')
    @endsection 

    @section('content') 
    <div class="search-div">
            <h1>How To Go</h1>
            
            <form role="search" method="post" class="search-form" action="{{ route('place.search') }}">
            @CSRF
                <div>
                    <label for="s" class="search-label">From</label>
                    <select id="place1" class="js-example-tags search-field " name="place1" placeholder="You want to go from ...">
                        <@foreach ($places_names as $name)
                            <option value="{{$name}}" {{ (old("place1") == $name ? "selected":"") }}>{{$name}}</option>
                            <!-- @if (old('place1') == $name)
                                <option value="{{$name}}" selected>{{$name}}</option>
                            @else
                                <option value="{{$name}}">{{$name}}</option>
                            @endif -->
                        @endforeach
                    </select>
                </div>
                <div>
                    <label for="s" class="search-label">To</label>
                    <select id="place2" class="js-example-tags search-field" name="place2" placeholder="You want to go to ...">
                        <@foreach ($places_names as $name)
                            <option value="{{$name}}" {{ (old("place2") == $name ? "selected":"") }}>{{$name}}</option>
                            <!-- @if (old('place2') == $name)
                                <option value="{{$name}}" selected>{{$name}}</option>
                            @else
                                <option value="{{$name}}">{{$name}}</option>
                            @endif        -->
                        @endforeach
                    </select>
                </div>
                <button class="button button-primary button-search">
                    <i class="dashicons dashicons-search"></i>
                    <span class="screen-reader-text">Search</span>
                </button>
            </form>
        </div>


        <div id="search-results" class="search-results">
            <p>Search results for how to go </p>
            <p>From {{$place1}} To {{$place2}}</p>
            @if ($places_result->count()==0)
                <p> No Results Found! </p>
            
            @else
                <ul style="list-style-type: none;">
                    @foreach ($places_result as $item)                                     
                        <li class="result-item">{{$item->discription}}</li>  
                    @endforeach 
                </ul>
            @endif
        </div>

    @endsection

    @section('footer')
        @include('main.footer')
    @endsection
    
    @section('scripts')
        <script src="{{asset('js/main.js')}}"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
        <script type="text/javascript">
           $(".js-example-tags").select2({
                tags: true
                });
        </script>
    @endsection
