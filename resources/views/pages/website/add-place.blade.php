@extends('main.main-layout')
@section('title','Ezzay Aro7')

@section('styles')
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="{{asset('images/add-place-form/icons/favicon.ico')}}"/>
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/add-place-form/bootstrap/css/bootstrap.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('fonts/add-place-form/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/add-place-form/animate/animate.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/add-place-form/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('vendor/add-place-form/select2/select2.min.css')}}">
    <!--===============================================================================================-->
        <link rel="stylesheet" type="text/css" href="{{asset('css/add-place-form/util.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('css/add-place-form/main.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/main.css')}}" >
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
@endsection

@section('header')
    @include('main.header')
@endsection 

@section('content') 
    <div class="contact1">
        <div class="container-contact1">
            <div class="contact1-pic js-tilt" data-tilt>
                <img src="{{asset('images/add-place-form/img-01.png')}}" alt="IMG">
            </div>
            <form method="post" class="contact1-form validate-form" action="{{ route('place_review.create') }}">
                @CSRF
                <span class="contact1-form-title">
                    Share your experience
                </span>
                <div class="contact1-form-title">
                <!-- أضف مكان انت ذهبت إليه وساعد الآخرين الذين يبحثون عن نفس المكان للوصول إليه بسهولة  -->
                    Share your experience and help who want to go to the place you visited and describe how you went to that place
                </div>
                <label for="other">Name</label>
                <div class="wrap-input1 validate-input" data-validate = "Name is required">
                    @auth
                    <input class="input1" type="text" name="name" placeholder="Your Name ... " value="{{ Auth::user()->name }}">
                    @endauth
                    @guest
                    <input class="input1" type="text" name="name" placeholder="Your Name ... " value="{{old('name')}}">
                    @endguest
                    <span class="shadow-input1"></span>
                </div>

                <label for="other">Email</label>
                <div class="wrap-input1 validate-input" data-validate = "Valid email is required: ex@abc.xyz">
                    
                    @auth
                    <input class="input1" type="text" name="email" placeholder="Your Email ..." value="{{ Auth::user()->email }}">
                    @endauth
                    @guest
                    <input class="input1" type="text" name="email" placeholder="Your Email ..." value="{{old('email')}}">
                    @endguest
                    
                    <span class="shadow-input1"></span>
                </div>

                <label for="other">Start Place</label>
                <div class="wrap-input1 validate-input" data-validate = "Subject is required"> 
                    <input class="input1" type="text" name="start_place" placeholder="Start PLace ..." value="{{old('start_place')}}">
                    <span class="shadow-input1"></span>
                </div>

                <label for="other">Distination Place</label>
                <div class="wrap-input1 validate-input" data-validate = "Subject is required">
                    <input class="input1" type="text" name="dist_place" placeholder="Start PLace ..." value="{{old('dist_place')}}">
                    <span class="shadow-input1"></span>
                </div>

                <label for="other">Description</label>
                <div class="wrap-input1 validate-input" data-validate = "Message is required">
                    <textarea class="input1" name="discription" placeholder="Description ..." >{{old('discription')}}</textarea>
                    <!-- <input class="input1" type="text" name="discription" placeholder="صف كيف وصلت إلى وجهتك ..."> -->
                    <span class="shadow-input1"></span>
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="container-contact1-form-btn">
                    <button class="contact1-form-btn" type="submit">
                        <span>
                            Submit for review
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                        </span>
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('footer')
    @include('main.footer')
@endsection

@section('scripts')    

    <!--===============================================================================================-->
        <script src="{{asset('vendor/add-place-form/jquery/jquery-3.2.1.min.js')}}"></script>
    <!--===============================================================================================-->
	    <script src="{{asset('vendor/add-place-form/bootstrap/js/popper.js')}}"></script>
	    <script src="{{asset('vendor/add-place-form/bootstrap/js/bootstrap.min.js')}}"></script>         
    <!--===============================================================================================-->
        <script src="{{asset('vendor/add-place-form/select2/select2.min.js')}}"></script>
    <!--===============================================================================================-->
        <script src="{{asset('vendor/add-place-form/tilt/tilt.jquery.min.js')}}"></script>
        <script >
            $('.js-tilt').tilt({
                scale: 1.1
            })
        </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23581568-13"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-23581568-13');
    </script>

    <!--===============================================================================================-->
        <script src="{{asset('js/add-place-form/main.js')}}"></script>
    
    <script src="{{asset('js/main.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

@endsection












