
<div class="navbar-wrapper">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="{{url('/')}}">
            <img class = "logo-img" src="{{ asset('images/logo.png') }}" alt="">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">
            <li class="nav-item btn btn-outline-success active">
                <a class="nav-link" href="{{url('/')}}">Home<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item btn btn-outline-success">
                <a class="nav-link" href="{{url('/add-place')}}" target="_blank">Share your experience</a>
            </li>
            <li class="nav-item btn btn-outline-success">
                <a class="nav-link" href="#" target="_blank">About Us</a>
            </li>
            <li class="nav-item btn btn-outline-success">
                <a class="nav-link" href="#" target="_blank">Contact Us</a>
            </li>
            <!-- <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown link
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <a class="dropdown-item" href="#">Something else here</a>
                </div>
            </li> -->
            </ul>
        </div>
    </nav>
</div>

