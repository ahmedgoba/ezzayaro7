<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title> @yield('title') </title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
            @yield('styles')     
    </head>
    <body>
        <header>
            @include('main.header')    
        </header>
        
        @yield('content')
        <footer>
            @include('main.footer')
        </footer>
    @yield('scripts')
    </body>
</html>