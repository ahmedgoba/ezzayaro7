<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/add-place', function () {
    return view('pages.website.add-place');
});

Auth::routes();

Route::get('/', 'HomeController@index');


Route::post('/place/get', 'PlaceController@search')->name('place.search');
Route::post('/place/create', 'PlaceController@create')->name('place.create');

Route::post('/review/create', 'PlacesReviewController@create')->name('place_review.create');
Route::get('/review/confirm/{id}', 'PlacesReviewController@confirm')->name('place_review.confirm');



Route::group(['prefix' => 'goba'], function () {
    Voyager::routes();
});
